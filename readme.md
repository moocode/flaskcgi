Flask CGI demo project
======================

This is a demo project showing how to deploy python flask project on shared hosting with cgi only.

Steps
-----

1. upload files from this repo to you main shared hosting dir (usually something like public_html)

2. replace ABSOLUTE_PATH_TO_YOUR_ACCOUNT in cgi-bin/cgi.py with your account dir absolute path

3. prepare *python* site-packages directory for yourapp with virtualenv:

	a) install virtualenv:
	
		sudo pip install virtualenv

	b) activate virtualenv:
			
		. python/bin/activate
				
	c) add flask dependency:
			
		pip install Flask

4. upload python dir to main directory

Test
----
Now browsing http://your_host/yourapp/ (trailing slash is important) you should see "Hello World!!"
and at http://your_host/yourapp/start "start" message


