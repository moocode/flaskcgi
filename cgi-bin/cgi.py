#!/usr/bin/python.cgi
# -*- coding: UTF-8 -*-
import sys
import site
site.addsitedir('ABSOLUTE_PATH_TO_YOUR_ACCOUNT/yourapp')

site.addsitedir('ABSOLUTE_PATH_TO_YOUR_ACCOUNT/python/lib/python2.7/site-packages')
import os

from wsgiref.handlers import CGIHandler
from yourapp import app
CGIHandler().run(app)
