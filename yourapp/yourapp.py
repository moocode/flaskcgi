from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello World!!'

@app.route('/start')
def start_route():
	return 'start'

if __name__ == "__main__":
	app.run()


